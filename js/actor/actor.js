/**
 * Extend the base Actor entity by defining a custom roll data structure which is ideal for the Simple system
 * @extends {Actor}
 */
export class DragonsHuntersActor extends Actor {

  /**
   * Augment the basic actor data with additional dynaic data
   */
  prepareData() {
    super.prepareData()

    const actorData = this.data
    const data = actorData.data
    const flags = actorData.flags

    if (actorData.type === 'character') this._prepareCharacterData(data)
  }

  /**
   * Prepare Character type specific data
   */
  _prepareCharacterData(data) {
    for (let [key, carac] of Object.entries(data.caracs)) {
      carac.mod = carac.value
    }
  }
}